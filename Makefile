NAME = minishell
CC = gcc
CFLAGS = -Wall -Werror -Wextra -O3
IFLAGS = -I includes -I $(LIBFT_FLD) -I $(FT_PRINTF_FLD)

LIBRARIES_FLD = libraries

LIBFT_FLD = $(LIBRARIES_FLD)/libft
LIBFT = $(LIBFT_FLD)/libft.a

FT_PRINTF_FLD = $(LIBRARIES_FLD)/ft_printf
FT_PRINTF = $(FT_PRINTF_FLD)/libftprintf.a

SOURCES_FLD_1 = sources
OBJECTS_FLD = objects

OBJECTS_1 =\
	$(OBJECTS_FLD)/ft_minish_main.o\
	$(OBJECTS_FLD)/ft_minish_service.o\
	$(OBJECTS_FLD)/ft_minish_lists_env.o\
	$(OBJECTS_FLD)/ft_minish_lists_env2.o\
	$(OBJECTS_FLD)/ft_minish_run_command.o\
	$(OBJECTS_FLD)/ft_minish_buildins_cd.o\
	$(OBJECTS_FLD)/ft_minish_buildins_env.o\
	$(OBJECTS_FLD)/ft_minish_buildins_echo.o\
	$(OBJECTS_FLD)/ft_minish_parse_command.o

.SILENT:

all : $(NAME)

$(NAME) : $(LIBFT) $(FT_PRINTF) $(OBJECTS_1)
	$(CC) $(IFLAGS) $(CFLAGS) $(OBJECTS_1) -L $(FT_PRINTF_FLD) -lftprintf  -L $(LIBFT_FLD) -lft -o $(NAME)

debug :
	make re -C . CFLAGS="$(CFLAGS) -Og -g"

$(OBJECTS_FLD)/ft_minish_main.o : $(SOURCES_FLD_1)/ft_minish_main.c
	@mkdir -p $(OBJECTS_FLD)
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_main.c -o $(OBJECTS_FLD)/ft_minish_main.o

$(OBJECTS_FLD)/ft_minish_run_command.o : $(SOURCES_FLD_1)/ft_minish_run_command.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_run_command.c -o $(OBJECTS_FLD)/ft_minish_run_command.o

$(OBJECTS_FLD)/ft_minish_buildins_cd.o : $(SOURCES_FLD_1)/ft_minish_buildins_cd.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_buildins_cd.c -o $(OBJECTS_FLD)/ft_minish_buildins_cd.o

$(OBJECTS_FLD)/ft_minish_buildins_env.o : $(SOURCES_FLD_1)/ft_minish_buildins_env.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_buildins_env.c -o $(OBJECTS_FLD)/ft_minish_buildins_env.o

$(OBJECTS_FLD)/ft_minish_buildins_echo.o : $(SOURCES_FLD_1)/ft_minish_buildins_echo.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_buildins_echo.c -o $(OBJECTS_FLD)/ft_minish_buildins_echo.o

$(OBJECTS_FLD)/ft_minish_service.o : $(SOURCES_FLD_1)/ft_minish_service.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_service.c -o $(OBJECTS_FLD)/ft_minish_service.o

$(OBJECTS_FLD)/ft_minish_lists_env.o : $(SOURCES_FLD_1)/ft_minish_lists_env.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_lists_env.c -o $(OBJECTS_FLD)/ft_minish_lists_env.o

$(OBJECTS_FLD)/ft_minish_lists_env2.o : $(SOURCES_FLD_1)/ft_minish_lists_env2.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_lists_env2.c -o $(OBJECTS_FLD)/ft_minish_lists_env2.o

$(OBJECTS_FLD)/ft_minish_parse_command.o : $(SOURCES_FLD_1)/ft_minish_parse_command.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_minish_parse_command.c -o $(OBJECTS_FLD)/ft_minish_parse_command.o

$(LIBFT) :
	echo "\tMaking\t\t$(LIBFT)"
	make -C $(LIBFT_FLD)
	echo "\tDone making\t$(LIBFT)"

$(FT_PRINTF) :
	echo "\tMaking\t\t$(FT_PRINTF)"
	make -C $(FT_PRINTF_FLD)
	echo "\tDone making\t$(FT_PRINTF)"

clean :
	echo "\tCleaning\t$(LIBFT) and $(FT_PRINTF)"
	make clean -C $(LIBFT_FLD)
	make clean -C $(FT_PRINTF_FLD)
	rm -f $(OBJECTS_1)

fclean : clean
	echo "\tFcleaning\t$(LIBFT) and $(FT_PRINTF)"
	make fclean -C $(LIBFT_FLD)
	make fclean -C $(FT_PRINTF_FLD)
	rm -f $(NAME)


re : fclean all

dre : dclean debug
