/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_main.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 16:31:10 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/20 10:49:56 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

/*
**	Updating paths to the binaries.
*/

static void	ft_minish_update_paths(t_minish_struct *command_info)
{
	t_minish_env_list	*path;

	path = ft_minish_list_env_get(command_info->environ_vars, "PATH");
	if (path)
		command_info->paths = ft_strsplit(path->data, ':');
	else
		command_info->paths = 0;
}

/*
**	Trimming the command(deleting tabs and spaces)
**	and checking if there is something besides whitespaces.
*/

static void	ft_minish_trim_command(t_minish_struct *command_info)
{
	command_info->command_path = ft_strtrim(command_info->command);
	ft_strdel(&(command_info->command));
	command_info->command = command_info->command_path;
	command_info->command_path = 0;
	if (!command_info->command || !command_info->command[0])
		ft_strdel(&(command_info->command));
}

/*
**	Creating parts of the main structure, and setting important fields to 0;
**	Copying env variables. Adding PROMPT variable.
*/

static void	ft_minish_fill_command_info(t_minish_struct *command_info,
							const char **env)
{
	size_t	ct;

	ct = 0;
	while (env[ct])
		ct++;
	command_info->paths = 0;
	command_info->command = 0;
	command_info->buildin = 0;
	command_info->command_path = 0;
	command_info->command_args = 0;
	command_info->environ_vars = 0;
	ct = 0;
	while (env[ct])
		ft_minish_list_env_add(&(command_info->environ_vars),
			ft_minish_list_env_new(env[ct++]));
	ft_minish_list_env_add(&(command_info->environ_vars),
		ft_minish_list_env_new_d("PROMPT", MINISHELL_DEFAULT_PROMPT));
	ft_minish_update_paths(command_info);
}

/*
**	Deleting everythin that was allocated.
**	Since command_info -> command is the same as command_info->command_args[0]
**	we don't delete it separately.
*/

static void	ft_minish_clear_command_info(t_minish_struct *command_info)
{
	size_t		ct;

	ft_strdel(&(command_info->command_path));
	ct = 0;
	while (command_info->command_args && command_info->command_args[ct])
		ft_strdel(&(command_info->command_args[ct++]));
	ft_memdel((void**)&(command_info->command_args));
	ct = 0;
	while (command_info->paths && command_info->paths[ct])
		ft_strdel(&(command_info->paths[ct++]));
	ft_memdel((void**)&(command_info->paths));
	command_info->buildin = 0;
	command_info->command = 0;
	command_info->command_args = 0;
	ft_minish_update_paths(command_info);
}

/*
**	Printing prompt and reading the line.
**	Running the command and clearing the stuff.
**	0 - is STDIN
*/

int			main(const int ac, const char **av, const char **env)
{
	t_minish_struct	command_info;

	if (ac > 2)
		ft_minish_print_error((char*)&(av[0][2]),
			"too many arguments", av[1], 1);
	command_info.prg_name = (char*)&(av[0][2]);
	ft_minish_fill_command_info(&command_info, env);
	while (1)
	{
		ft_printf("%r%s%r ", av[1],
		ft_minish_list_env_get(command_info.environ_vars, "PROMPT")
		? ft_minish_list_env_get(command_info.environ_vars, "PROMPT")->data
		: MINISHELL_DEFAULT_PROMPT, "reset");
		if (get_next_line(0, &(command_info.command)) < 0)
			break ;
		ft_minish_trim_command(&command_info);
		if (command_info.command && command_info.command[0])
			ft_minish_run_command(&command_info);
		ft_minish_clear_command_info(&command_info);
	}
	exit(EXIT_SUCCESS);
}
