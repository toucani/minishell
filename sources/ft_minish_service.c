/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_service.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 11:12:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 16:19:30 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

void		ft_minish_exit(const int code)
{
	exit(code);
}

/*
**	Printing error, if the exit_i is not 0 - exiting.
*/

void		ft_minish_print_error(const char *prg_name, const char *msg,
					const char *msg2, int exit_i)
{
	ft_putstr_fd(prg_name, 2);
	ft_putstr_fd(": ", 2);
	ft_putstr_fd(msg, 2);
	if (msg2)
	{
		ft_putstr_fd(": ", 2);
		ft_putstr_fd(msg2, 2);
	}
	ft_putchar_fd('\n', 2);
	if (exit_i)
		ft_minish_exit(EXIT_FAILURE);
}
