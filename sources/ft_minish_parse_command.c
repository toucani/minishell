/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_parse_command.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 13:21:53 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/17 13:45:58 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

/*
**	Checking the amount of doube quotes in a line
*/

static int			check_qts(const char *str)
{
	char			*last_qt;
	unsigned int	qt_am;

	qt_am = 0;
	last_qt = (char*)str;
	while (ft_strchr(last_qt, '"') && ++qt_am)
		last_qt = ft_strchr(last_qt, '"') + 1;
	return (!(qt_am % 2));
}

/*
**	Counting separate "words" in a line
**	N.B: something between quotations marks - is one word.
*/

static unsigned int	count_words(const char *str)
{
	size_t			ct;
	unsigned int	words_am;

	ct = 0;
	words_am = 0;
	while (str[ct])
	{
		while (str[ct] && ft_isspc(str[ct]))
			ct++;
		if (str[ct])
			words_am++;
		if (str[ct] && str[ct] == '"' && ft_strchr(&(str[ct + 1]), '"'))
			ct = ft_strchr(&(str[ct + 1]), '"') - str + 1;
		else
			while (str[ct] && !ft_isspc(str[ct]))
				ct++;
	}
	return (words_am);
}

static char			*ft_minish_split_command_get_next_word(const char *str,
						size_t start, size_t end)
{
	start += (str[start] == '"') ? 1 : 0;
	if (end != start)
		return (
			ft_strsub(str, start,
			end - start - ((str[end - 1] == '"') ? 1 : 0)));
	else
		return (0);
}

/*
**	Splitting the line into array of nul-terminated words,
**		ommiting quotation marks.
**	N.B: something between quotations marks - is one word.
**	words_am[0] - total words
**	words_am[1] = current word
*/

static char			**ft_minish_split_command(const char *str)
{
	unsigned int	words_am[2];
	char			**words;
	size_t			start;
	size_t			ct;

	ct = 0;
	start = 0;
	words_am[1] = 0;
	words_am[0] = count_words(str);
	words = (char**)ft_memalloc(sizeof(char*) * (words_am[0] + 1));
	while (str[ct] && words_am[1] < words_am[0])
	{
		while (str[ct] && ft_isspc(str[ct]))
			ct++;
		start = (str[ct]) ? ct : start;
		if (str[ct] && str[ct] == '"' && ft_strchr(&(str[ct + 1]), '"'))
			ct = ft_strchr(&(str[ct + 1]), '"') - str + 1;
		else
			while (str[ct] && !ft_isspc(str[ct]))
				ct++;
		words[words_am[1]++] =
			ft_minish_split_command_get_next_word(str, start, ct);
	}
	return (words);
}

/*
**	Checking that the command can be divided into "words", if yes -> diving it.
**	If the amount of quotation marks in not even,
**	reads more input unit the amount of qts in even.
*/

void				ft_minish_parse_command(t_minish_struct *command_info)
{
	char	*temp;

	while (!check_qts(command_info->command))
	{
		temp = 0;
		ft_printf("dquote > ");
		if (get_next_line(0, &temp) < 0)
			break ;
		command_info->command =
			ft_strjoin_del_first(&(command_info->command), "\n");
		command_info->command =
			ft_strjoin_ultimate(&(command_info->command), &temp);
	}
	command_info->command_args = ft_minish_split_command(command_info->command);
	ft_strdel(&(command_info->command));
	command_info->command = command_info->command_args[0];
	ft_strtolow(command_info->command);
}
