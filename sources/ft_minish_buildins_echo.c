/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_buildins_echo.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 12:37:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 11:20:16 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

/*
**	Gettin the var name out of the line, which can contain not spaces
*/

static char	*ft_minish_buildin_echo_get_var_name(const char *str)
{
	size_t	ct;
	char	*rt;
	char	*temp;

	ct = 0;
	temp = ft_strchr(str, '$');
	temp += (temp) ? 1 : 0;
	while (temp[ct] && !ft_isspc(temp[ct]))
		ct++;
	rt = ft_strsub(temp, 0, ct);
	return (rt);
}

/*
**	Looking for an env variable with this name and printing ins data.
*/

static void	ft_minish_buildin_echo_print_var(const char *str,
								t_minish_env_list *env)
{
	char				*temp;
	char				*var_name;
	t_minish_env_list	*elem;

	temp = (char*)str;
	while (ft_strchr(temp, '$'))
	{
		ft_printf("%.*s", ft_strchr(temp, '$') ? (size_t)
			(ft_strchr(temp, '$') - temp) :
			(size_t)(ft_strlen(temp)), temp);
		temp = ft_strchr(temp, '$');
		if (!ft_isspc(temp[1]))
		{
			var_name = ft_minish_buildin_echo_get_var_name(temp);
			elem = ft_minish_list_env_get(env, var_name);
			ft_printf("%s", elem ? elem->data : "");
			temp += ft_strlen(var_name) + 1;
			ft_strdel(&var_name);
		}
		else
			ft_printf("%c", *temp++);
	}
	ft_printf("%s ", temp ? temp : "");
}

void		ft_minish_buildin_echo(t_minish_struct *command_info)
{
	size_t				ct;

	ct = 0;
	while (command_info->command_args[++ct])
	{
		if (ft_strchr(command_info->command_args[ct], '$'))
			ft_minish_buildin_echo_print_var(command_info->command_args[ct],
						command_info->environ_vars);
		else
			ft_printf("%s ", command_info->command_args[ct]);
	}
	ft_putchar('\n');
}
