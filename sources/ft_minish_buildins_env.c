/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_buildins_env.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 10:48:14 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 11:16:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

/*
**	Printitng the list of variables
*/

void	ft_minish_buildin_env(t_minish_struct *command_info)
{
	t_minish_env_list	*variable;

	if (!command_info->command_args[1])
	{
		variable = command_info->environ_vars;
		while (variable)
		{
			ft_printf("%s=%s\n", variable->name,
				variable->data ? variable->data : "");
			variable = variable->next;
		}
	}
	else
		ft_minish_print_error(command_info->command, "too many arguments",
			command_info->command_args[1], 0);
}

/*
**	Deleting one of the variables from the list. Can handle many arguments.
*/

void	ft_minish_buildin_unsetenv(t_minish_struct *command_info)
{
	size_t	ct;

	if (!(command_info->command_args[1]))
		ft_minish_print_error(command_info->command, "too few arguments", 0, 0);
	ct = 0;
	while (command_info->command_args[++ct])
		ft_minish_list_env_remove(&command_info->environ_vars,
			command_info->command_args[ct]);
}

/*
**	Creating/changing one of the variables.
*/

void	ft_minish_buildin_setenv(t_minish_struct *command_info)
{
	t_minish_env_list	*the_element;

	if (!(command_info->command_args[1]))
		ft_minish_buildin_env(command_info);
	else if (command_info->command_args[2] && command_info->command_args[3])
		ft_minish_print_error(command_info->command, "too many arguments",
			command_info->command_args[3], 0);
	else
	{
		the_element = ft_minish_list_env_get(command_info->environ_vars,
			command_info->command_args[1]);
		if (the_element)
		{
			ft_strdel(&(the_element->data));
			the_element->data = ft_strdup(command_info->command_args[2]);
		}
		else
			ft_minish_list_env_add(&(command_info->environ_vars),
				ft_minish_list_env_new_d(command_info->command_args[1],
					command_info->command_args[2]));
	}
}
