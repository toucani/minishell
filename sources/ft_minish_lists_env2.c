/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_lists_env2.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:41:27 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 15:54:04 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell_lists.h"

/*
**	Removing ont of the elements, and relinking the rest.
*/

void	ft_minish_list_env_remove(t_minish_env_list **head,
						const char *name)
{
	t_minish_env_list	*temp;
	t_minish_env_list	*delete;
	t_minish_env_list	*prev;

	prev = 0;
	temp = *head;
	while (temp)
	{
		delete = 0;
		if (ft_strequ(temp->name, name))
		{
			if (prev)
				prev->next = temp->next;
			else
				*head = temp->next;
			delete = temp;
			temp = temp->next;
			ft_minish_list_env_del(&delete);
		}
		else
		{
			prev = temp;
			temp = temp->next;
		}
	}
}

/*
**	Generating an array of null-terminated string out of list of env varibles.
*/

char	**ft_minish_list_env_to_arr(t_minish_env_list *list)
{
	size_t				ct;
	t_minish_env_list	*temp;
	char				**rt;

	ct = 0;
	temp = list;
	while (temp && ++ct)
		temp = temp->next;
	rt = (char**)ft_memalloc(sizeof(char*) * (ct + 1));
	ct = 0;
	while (list)
	{
		if (ft_strequ(list->name, "PROMPT"))
			list = list->next;
		if (!list)
			break ;
		rt[ct++] = ft_strjoin_f(list->name, "=", list->data, 0);
		list = list->next;
	}
	return (rt);
}
