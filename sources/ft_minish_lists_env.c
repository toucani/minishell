/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_lists_env.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:41:27 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 11:13:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell_lists.h"

/*
**	Creating a new env element. Parametr is expected to be "NAME=DATA"
*/

t_minish_env_list	*ft_minish_list_env_new(const char *env)
{
	t_minish_env_list	*new;

	new = (t_minish_env_list*)ft_memalloc(sizeof(t_minish_env_list));
	new->name = ft_strsub(env, 0, ft_strchr(env, '=') - env);
	new->data = ft_strdup(ft_strchr(env, '=') + 1);
	return (new);
}

/*
**	Creating a new env element. Separate data and name parametrs.
*/

t_minish_env_list	*ft_minish_list_env_new_d(const char *name,
												const char *data)
{
	t_minish_env_list	*new;

	new = (t_minish_env_list*)ft_memalloc(sizeof(t_minish_env_list));
	new->name = ft_strdup(name);
	new->data = ft_strdup(data);
	return (new);
}

/*
**	Function to delete 1 element
*/

void				ft_minish_list_env_del(t_minish_env_list **element)
{
	ft_strdel(&((*element)->name));
	ft_strdel(&((*element)->data));
	ft_memdel((void**)element);
}

/*
**	Pushing back into the list
*/

void				ft_minish_list_env_add(t_minish_env_list **head,
						t_minish_env_list *new)
{
	t_minish_env_list	*temp;

	temp = *head;
	if (!(*head))
		*head = new;
	else
	{
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
}

/*
**	Looking in the list for a variable with a given NAME.
**	If cant find, returns 0;
*/

t_minish_env_list	*ft_minish_list_env_get(t_minish_env_list *head,
								const char *name)
{
	while (head)
		if (ft_strequ(head->name, name))
			return (head);
		else
			head = head->next;
	return (0);
}
