/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_lists.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:36:37 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/13 12:32:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MINISHELL_LISTS_H
# define MINISHELL_LISTS_H

# include "libft.h"

/*
**	List to have the environment variables in.
*/

typedef struct					s_minish_env_list
{
	char						*name;
	char						*data;
	struct s_minish_env_list	*next;
}								t_minish_env_list;

t_minish_env_list				*ft_minish_list_env_new(const char *env);
t_minish_env_list				*ft_minish_list_env_new_d(const char *name,
															const char *data);
void							ft_minish_list_env_del(t_minish_env_list
																	**element);
void							ft_minish_list_env_add(t_minish_env_list **head,
														t_minish_env_list *new);
t_minish_env_list				*ft_minish_list_env_get(t_minish_env_list *head,
															const char *name);
void							ft_minish_list_env_remove(t_minish_env_list
													**head, const char *name);
char							**ft_minish_list_env_to_arr(t_minish_env_list
																	*list);

#endif
