/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 16:36:05 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 13:40:35 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include "ft_printf.h"
# include "get_next_line.h"
# include "minishell_lists.h"

# include <unistd.h>
# include <stdlib.h>

/*
**	The prompt to print. Default is "$> "
*/

# define MINISHELL_DEFAULT_PROMPT "Your command, master:"

/*
**	Just to make life easier, and make it possible to change the commands
**	to invoke buildins. Do i really need it???
**	ft_minish_buildin_ - is default name of the buildin function
*/

# define MINISHELL_BUILDINS "exit echo cd env setenv unsetenv"
# define IS_BUILDIN_EXIT(str) ft_strequ(str, "exit")
# define IS_BUILDIN_ECHO(str) ft_strequ(str, "echo")
# define IS_BUILDIN_CD(str) ft_strequ(str, "cd")
# define IS_BUILDIN_ENV(str) ft_strequ(str, "env")
# define IS_BUILDIN_SETENV(str) ft_strequ(str, "setenv")
# define IS_BUILDIN_USETENV(str) ft_strequ(str, "unsetenv")
# define GLUE(x, y) x##y
# define MINISHELL_BUILDIN_FT(name) GLUE(ft_minish_buildin_, name)

/*
**	prg_name	-	  name of the binary file(ussually "minishell")
**	command		-	  initially the string, read from the tereminal,
**						after that, only the first word of that string.
**	command_path	- path to the binary command, if the command is not buildin
**	command args	- the initial string from the terminal, splitted into words
**						(with the first word)
**	paths			- paths to folders with binaries
**	environ_vars	- copy of environ vars, which can be changed
**	buildin			- pointer to a buildin function
*/

typedef struct			s_minish_struct
{
	char				*prg_name;
	char				*command;
	char				*command_path;
	char				**command_args;
	char				**paths;
	t_minish_env_list	*environ_vars;
	void				(*buildin)(struct s_minish_struct *command_info);
}						t_minish_struct;

void					ft_minish_exit(const int code);
void					ft_minish_print_error(const char *prg_name,
							const char *msg, const char *msg2, int exit_i);
void					ft_minish_parse_command(t_minish_struct *command_info);
void					ft_minish_run_command(t_minish_struct *command_info);
void					ft_minish_buildin_cd(t_minish_struct *comm_info);
void					ft_minish_buildin_echo(t_minish_struct *comm_info);
void					ft_minish_buildin_env(t_minish_struct *comm_info);
void					ft_minish_buildin_setenv(t_minish_struct *comm_info);
void					ft_minish_buildin_unsetenv(t_minish_struct *comm_info);

#endif
