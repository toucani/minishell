/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_buildins_cd.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 10:20:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/20 10:44:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"

/*
**	Checking if all the arguments are valid.
**	If the user wants to use HOME path or OLDPWD path, checking that
**	they exist.
*/

static int	ft_minish_buildin_cd_check(t_minish_struct *command_info)
{
	t_minish_env_list	*home_var;
	t_minish_env_list	*oldpwd_var;

	home_var = ft_minish_list_env_get(command_info->environ_vars, "HOME");
	oldpwd_var = ft_minish_list_env_get(command_info->environ_vars, "OLDPWD");
	if (command_info->command_args[1] && command_info->command_args[2])
		ft_minish_print_error(command_info->command, "too many arguments",
			command_info->command_args[2], 0);
	else if ((!home_var || !home_var->data) && (!command_info->command_args[1]
		|| ft_strchr(command_info->command_args[1], '~')))
		ft_minish_print_error(command_info->command,
			"cannot find HOME path", "check HOME variable", 0);
	else if ((!oldpwd_var || !oldpwd_var->data) &&
		command_info->command_args[1][0] == '-' &&
		!command_info->command_args[1][1])
		ft_minish_print_error(command_info->command,
			"cannot find OLDPWD path", "check OLDPWD variable", 0);
	else
		return (1);
	return (0);
}

/*
**	Generating the full path to change.
*/

static void	ft_minish_buildin_cd_get_path(t_minish_struct *command_info)
{
	t_minish_env_list	*home_var;
	t_minish_env_list	*oldpwd_var;

	home_var = ft_minish_list_env_get(command_info->environ_vars, "HOME");
	oldpwd_var = ft_minish_list_env_get(command_info->environ_vars, "OLDPWD");
	if (!command_info->command_args[1])
		command_info->command_path = ft_strdup(home_var->data);
	else if (ft_strchr(command_info->command_args[1], '~'))
		command_info->command_path = ft_strjoin_f(ft_strsub(
			command_info->command_args[1], 0,
			ft_strchr(command_info->command_args[1], '~') -
			command_info->command_args[1]), home_var->data,
			ft_strchr(command_info->command_args[1], '~') + 1, "");
	else if (command_info->command_args[1][0] == '-' &&
		!command_info->command_args[1][1])
	{
		command_info->command_path = ft_strdup(oldpwd_var->data);
		ft_printf("%s%s\n", (ft_strstr(oldpwd_var->data, home_var->data)) ?
		"~" : "", (ft_strstr(oldpwd_var->data, home_var->data)) ?
		&(oldpwd_var->data[ft_strlen(home_var->data)]) : oldpwd_var->data);
	}
	else
		command_info->command_path = ft_strdup(command_info->command_args[1]);
}

/*
**	Checking the access to the generated path.
*/

static int	ft_minish_buildin_cd_check_access(t_minish_struct *command_info)
{
	if (access(command_info->command_path, X_OK) != 0)
	{
		ft_minish_print_error(command_info->command, "permission denied",
			command_info->command_path, 0);
		ft_strdel(&(command_info->command_path));
		return (0);
	}
	return (1);
}

/*
**	Updating PWD and OLDPWD(or creating them)
*/

static void	ft_minish_buildin_cd_update_env(t_minish_struct *command_info)
{
	t_minish_env_list	*pwd;
	t_minish_env_list	*opwd;

	if (!(opwd = ft_minish_list_env_get(command_info->environ_vars, "OLDPWD")))
	{
		opwd = ft_minish_list_env_new_d("OLDPWD", 0);
		ft_minish_list_env_add(&(command_info->environ_vars), opwd);
	}
	if (!(pwd = ft_minish_list_env_get(command_info->environ_vars, "PWD")))
	{
		pwd = ft_minish_list_env_new_d("PWD", 0);
		ft_minish_list_env_add(&(command_info->environ_vars), pwd);
	}
	ft_strdel(&opwd->data);
	opwd->data = pwd->data;
	pwd->data = getcwd(0, 0);
}

/*
**	Checking if the command is valid, parsing the path, checking access,
**	and changing the dir.
*/

void		ft_minish_buildin_cd(t_minish_struct *command_info)
{
	if (!ft_minish_buildin_cd_check(command_info))
		return ;
	ft_minish_buildin_cd_get_path(command_info);
	if (!ft_minish_buildin_cd_check_access(command_info))
		return ;
	if (chdir(command_info->command_path) < 0)
		ft_minish_print_error(command_info->command, "cannot change path",
			command_info->command_path, 0);
	else
		ft_minish_buildin_cd_update_env(command_info);
	ft_strdel(&(command_info->command_path));
}
