/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minish_run_command.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 10:18:39 by dkovalch          #+#    #+#             */
/*   Updated: 2017/03/15 11:04:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of minishell project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "minishell.h"
#include <sys/wait.h>
#include <dirent.h>

/*
**	Checks if the command can be run as a buildin command
*/

static int	ft_minish_check_buildin(t_minish_struct *command_info)
{
	if (!ft_strstr(MINISHELL_BUILDINS, command_info->command))
		return (0);
	else if (IS_BUILDIN_EXIT(command_info->command))
		ft_minish_exit(EXIT_SUCCESS);
	else if (IS_BUILDIN_ECHO(command_info->command))
		command_info->buildin = MINISHELL_BUILDIN_FT(echo);
	else if (IS_BUILDIN_CD(command_info->command))
		command_info->buildin = MINISHELL_BUILDIN_FT(cd);
	else if (IS_BUILDIN_ENV(command_info->command))
		command_info->buildin = MINISHELL_BUILDIN_FT(env);
	else if (IS_BUILDIN_SETENV(command_info->command))
		command_info->buildin = MINISHELL_BUILDIN_FT(setenv);
	else if (IS_BUILDIN_USETENV(command_info->command))
		command_info->buildin = MINISHELL_BUILDIN_FT(unsetenv);
	return (command_info->buildin ? 1 : 0);
}

/*
**	Looking for a command in system folders
*/

static int	ft_minish_check_command(t_minish_struct *command_info)
{
	DIR				*directory;
	struct dirent	*element;
	char			**path;

	command_info->command_path = 0;
	path = command_info->paths;
	while (path && *path && !(command_info->command_path))
	{
		if (access(*path, X_OK) == 0)
			if ((directory = opendir(*path)))
			{
				while ((element = readdir(directory)))
					if (ft_strequ(command_info->command, element->d_name))
					{
						command_info->command_path =
							ft_strjoin_f(*path, "/", command_info->command, "");
						break ;
					}
				closedir(directory);
			}
		path++;
	}
	return ((command_info->command_path ||
		ft_strnequ(command_info->command, "./", 2) ||
		command_info->command[0] == '/') ? 1 : 0);
}

/*
**	Checking access to the command
*/

static int	ft_minish_check_access(t_minish_struct *command_info)
{
	char	*temp;

	temp = command_info->command_path ? command_info->command_path :
		command_info->command;
	if (access(temp, X_OK) != 0)
	{
		ft_minish_print_error(command_info->command, "permission denied",
			command_info->command_path, 0);
		return (0);
	}
	return (1);
}

/*
**	Main function, parses the command, find buildin command (if there is one),
**	finds system command and runs it, or prints error message.
*/

void		ft_minish_run_command(t_minish_struct *command_info)
{
	int pid;

	ft_minish_parse_command(command_info);
	if (ft_minish_check_buildin(command_info))
		(*command_info->buildin)(command_info);
	else if (ft_minish_check_command(command_info))
	{
		if (!ft_minish_check_access(command_info))
			return ;
		pid = fork();
		if (pid == 0)
		{
			if (execve(command_info->command_path ? command_info->command_path :
				command_info->command, command_info->command_args,
				ft_minish_list_env_to_arr(command_info->environ_vars)))
				ft_minish_print_error(command_info->prg_name,
				"error creating a new process", "execve call failed", 0);
			else
				ft_minish_exit(EXIT_SUCCESS);
		}
		wait(&pid);
	}
	else
		ft_minish_print_error(command_info->command,
			"command not found", 0, 0);
}
